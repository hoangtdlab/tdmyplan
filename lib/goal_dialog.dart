import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'goal_data.dart';
import 'user.dart';

class GoalDialog extends StatefulWidget {
  GoalData data;

  GoalDialog(this.data);

  @override
  State<StatefulWidget> createState() => _GoalDialogState();
}

class _GoalDialogState extends State<GoalDialog> {
  TextEditingController controller;
  bool isAutoDeposit;

  @override
  void initState() {
    super.initState();
    controller =
        TextEditingController(text: widget.data.suggested.toStringAsFixed(2));
    isAutoDeposit = widget.data.isAutoDeposit;
  }

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context);
    return AlertDialog(
      title: Text(
        widget.data.name,
        style: TextStyle(fontWeight: FontWeight.w700, fontSize: 20),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                "Auto Deposit",
              ),
              Switch(
                  activeColor: Colors.green,
                  value: isAutoDeposit,
                  onChanged: (res) {
                    setState(() {
                      isAutoDeposit = res;
                    });
                  }),
            ],
          ),
          Text(
            "Deposit",
          ),
          TextField(
            keyboardType: TextInputType.number,
            controller: controller,
          ),
          SizedBox(height: 16),
          Text("Every Month"),
          SizedBox(height: 16),
          Text("To"),
          DropdownButton<String>(
            value: "TaxFree",
            onChanged: (value) {},
            isExpanded: true,
            items: [
              DropdownMenuItem<String>(
                value: "TaxFree",
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 0),
                  child: Text(
                    "12345 Tax Free Saving Account",
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              )
            ],
          )
        ],
      ),
      actions: <Widget>[
        new FlatButton(
          child: new Text("Cancel"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        // usually buttons at the bottom of the dialog
        new FlatButton(
          child: new Text("Done"),
          onPressed: () {
            widget.data.suggested = double.parse(controller.text);
            widget.data.isAutoDeposit = isAutoDeposit;
            user.updateData();
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
