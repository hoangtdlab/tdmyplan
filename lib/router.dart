import 'form.dart';
import 'main.dart';
import 'home.dart';
import 'new_goal.dart';

class RouteNames {
  static const String initial = '/';
  static const String onboarding = '/onboarding';
  static const String home = '/home';
  static const String addGoalBig = '/addGoalBig';
  static const String addGoalSmall = '/addGoalBig';
}

class Router {
  static final routes = {
    RouteNames.initial: (context) => Welcome(),
    RouteNames.onboarding: (context) => FinancialSurvey(),
    RouteNames.home: (context) => Home(),
    RouteNames.addGoalBig: (context) => AddGoal(),
    RouteNames.addGoalSmall: (context) => AddGoal(isBig: false,),
  };
}
