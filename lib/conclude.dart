import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'nextprevbutton.dart';
import 'user.dart';

class Conclude extends StatefulWidget {
  final Function() nextStep, prevStep;

  Conclude(this.prevStep, this.nextStep);

  @override
  State<StatefulWidget> createState() => _ConcludeState();
}

class _ConcludeState extends State<Conclude> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Thanks ${user.name}. We created a personalized plan for you.",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
          ),
        ),
        SizedBox(
          height: 40,
        ),
        NavigationButton(
          widget.prevStep,
          () {
            widget.nextStep();
            User user = Provider.of<User>(context);
            user.tuition = double.parse(controller.text);
            user.updateData();
          },
          nextText: "Finish",
        )
      ],
    );
  }
}
