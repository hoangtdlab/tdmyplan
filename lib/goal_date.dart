import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:provider/provider.dart';

import 'nextprevbutton.dart';
import 'user.dart';

class GoalDate extends StatefulWidget {
  final Function() nextStep, prevStep;

  GoalDate(this.prevStep, this.nextStep);

  @override
  State<StatefulWidget> createState() => _GoalDateState();
}

class _GoalDateState extends State<GoalDate> {
  DateTime chosenDate = DateTime.now();

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context, listen: false);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        RichText(
          text: TextSpan(
            text: 'You want to ',
            style: DefaultTextStyle.of(context).style.copyWith(
                  fontWeight: FontWeight.w700,
                  fontSize: 20,
                ),
            children: <TextSpan>[
              TextSpan(
                text: (user.goalName.length > 0)
                    ? '${user.goalName[0].toLowerCase() + user.goalName.substring(1)}'
                    : '',
                style: DefaultTextStyle.of(context).style.copyWith(
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                      color: Colors.green,
                    ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 40,
        ),
        Text(
          "When do you want to achieve it?",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 16,
          ),
        ),
        SizedBox(
          height: 8,
        ),
        RaisedButton(
          color: Colors.white,
          onPressed: () async {
            try {
              DateTime newDate = await showMonthPicker(
                  context: context, initialDate: chosenDate, firstDate: DateTime.now());
              if (newDate == null) newDate = DateTime.now();
              setState(() {
                chosenDate = newDate;
              });
            } catch (e) {}
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(DateFormat.yMMMM().format(chosenDate)),
              Icon(Icons.expand_more)
            ],
          ),
        ),
        SizedBox(
          height: 40,
        ),
        NavigationButton(widget.prevStep, () {
          widget.nextStep();
          User user = Provider.of<User>(context);
          user.goalDate = chosenDate;
          user.updateData();
        })
      ],
    );
  }
}
