import 'package:flutter/material.dart';

import 'conclude.dart';
import 'education.dart';
import 'education_date.dart';
import 'first_goal.dart';
import 'goal_date.dart';
import 'goal_name.dart';
import 'goal_save.dart';
import 'goal_target.dart';
import 'income.dart';
import 'link_bank.dart';
import 'name.dart';
import 'rent.dart';
import 'router.dart';
import 'tuition.dart';

class FinancialSurvey extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _FinancialSurveyState();
}

class _FinancialSurveyState extends State<FinancialSurvey> {
  int step = 0;
  final steps = [
    (prevStep, nextStep) => NameChange(prevStep, nextStep),
    (prevStep, nextStep) => Education(prevStep, nextStep),
    (prevStep, nextStep) => EduDate(prevStep, nextStep),
    (prevStep, nextStep) => Income(prevStep, nextStep),
    (prevStep, nextStep) => Rent(prevStep, nextStep),
    (prevStep, nextStep) => Tuition(prevStep, nextStep),
    (prevStep, nextStep) => LinkBank(prevStep, nextStep),
    (prevStep, nextStep) => GoalName(prevStep, nextStep),
    (prevStep, nextStep) => GoalDate(prevStep, nextStep),
    (prevStep, nextStep) => GoalTarget(prevStep, nextStep),
    (prevStep, nextStep) => GoalSave(prevStep, nextStep),
    (prevStep, nextStep) => FirstGoal(prevStep, nextStep),
    (prevStep, nextStep) => Conclude(prevStep, nextStep),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        // or use Brightness.dark
        bottom: PreferredSize(
          preferredSize: Size(double.infinity, 1.0),
          child: LinearProgressIndicator(
            value: step / steps.length,
            backgroundColor: Colors.grey,
            valueColor: AlwaysStoppedAnimation<Color>(Colors.black38),
          ),
        ),
        centerTitle: true,
        title: Text(
          'MyPlan',
          style: TextStyle(
            fontWeight: FontWeight.w700,
          ),
        ),
        backgroundColor: Colors.green,
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          vertical: 24,
          horizontal: 24,
        ),
        child: Container(
          key: Key("Step " + step.toString()),
          child: steps[step](
            (step == 0)
                ? null
                : () {
                    setState(() {
                      step = step - 1;
                    });
                  },
            () {
              if (step == steps.length - 1) {
                Navigator.pushReplacementNamed(context, RouteNames.home);
              } else {
                setState(() {
                  step = step + 1;
                });
              }
            },
          ),
        ),
      ),
    );
  }
}
