import 'package:flutter/material.dart';

class NavigationButton extends StatelessWidget {
  Function() next, prev;

  final String nextText;

  NavigationButton(this.prev, this.next, {this.nextText = "Continue"});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        if (prev == null) Text(""),
        if (prev != null)
          RaisedButton(
            textColor: Colors.green,
            color: Colors.white,
            shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.green),
              borderRadius: BorderRadius.circular(30.0),
            ),
            child: Text("Back"),
            onPressed: prev,
          ),
        RaisedButton(
          textColor: Colors.white,
          color: Colors.green,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          child: Text(nextText),
          onPressed: () {
            if (next != null) next();
          },
        ),
      ],
    );
  }
}
