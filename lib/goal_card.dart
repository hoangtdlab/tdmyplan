import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'goal_data.dart';
import 'user.dart';

class GoalCard extends StatelessWidget {
  final GoalData data;
  bool template;

  GoalCard(this.data, {this.template = false});

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context, listen: false);
    return Container(
        height: 190,
        width: double.infinity,
        decoration: new BoxDecoration(
          color: Colors.purple, //new Color.fromRGBO(255, 0, 0, 0.0),
          borderRadius: new BorderRadius.all(const Radius.circular(15.0)),
        ),
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                data.name,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 16,
                ),
              ),
              SizedBox(height: 8),
              Text(
                "Complete by ${DateFormat.yMMMM().format(data.date)}",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                ),
              ),
              SizedBox(height: 8),
              Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(15.0),
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.65,
                      child: LinearProgressIndicator(
                        value: min(1, data.save / data.target),
                        backgroundColor: Colors.grey,
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                      ),
                    ),
                  ),
                  SizedBox(width: 8),
                  Text(
                    "${(min(data.save / data.target, 1.0) * 100).toInt()}%",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8),
              Text(
                data.isAutoDeposit ? "Auto Deposit Active" : "RECOMMENDED",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 14,
                ),
              ),
              if (template || data.isAutoDeposit) SizedBox(height: 8),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Deposit \$${data.suggested.toStringAsFixed(2)} a month",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                  ),
                  if (!template && !data.isAutoDeposit)
                    ButtonTheme(
                      height: 25.0,
                      child: RaisedButton(
                        textColor: Colors.white,
                        color: Colors.green,
                        disabledColor: Colors.grey,
                        disabledTextColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        child:
                            Text(data.haveDeposited ? "Deposited" : "Deposit"),
                        onPressed: data.haveDeposited
                            ? null
                            : () {
                                data.deposit();
                                user.updateData();
                              },
                      ),
                    ),
                ],
              )
            ],
          ),
        ));
  }
}
