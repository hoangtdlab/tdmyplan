import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'router.dart';
import 'user.dart';

void main() => runApp(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(builder: (_) => User.currentUser),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            fontFamily: 'Open Sans',
            cursorColor: Colors.green,
          ),
          routes: Router.routes,
        ),
      ),
    );

class Welcome extends StatefulWidget {
  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  int currentIndexPage = 0;
  int pageLength = 4;
  PageController pageController;

  @override
  void initState() {
    pageController = PageController(initialPage: pageLength * 10000);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          PageView.builder(
            itemBuilder: (context, index) {
              return Walkthrougth(
                  textContent: "");
            },
            controller: pageController,
            onPageChanged: (value) {
              setState(() => currentIndexPage = value % pageLength);
            },
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "TD MyPlan",
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(height: 250),
                  DotsIndicator(
                    numberOfDot: pageLength,
                    position: currentIndexPage,
                    dotColor: Colors.white30,
                    dotActiveColor: Colors.white,
                  ),
                  SizedBox(height: 16),
                  Text(
                    "Planning for post-secondary, marriage, or even home ownership? Complete the survey to find the plan for you.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  SizedBox(height: 16),
                  RaisedButton(
                    child: Text(
                      "Begin",
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    textColor: Colors.green,
                    color: Colors.white,
                    onPressed: () {
                      Navigator.pushReplacementNamed(
                          context, RouteNames.onboarding);
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class Walkthrougth extends StatelessWidget {
  final String textContent;

  Walkthrougth({Key key, @required this.textContent}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.green),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Center(child: Text(textContent)),
    );
  }
}
