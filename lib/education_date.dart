import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:provider/provider.dart';

import 'nextprevbutton.dart';
import 'user.dart';

class EduDate extends StatefulWidget {
  final Function() nextStep, prevStep;

  EduDate(this.prevStep, this.nextStep);

  @override
  State<StatefulWidget> createState() => _EduDateState();
}

class _EduDateState extends State<EduDate> {
  DateTime chosenDate = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "When's your expected education?",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
          ),
        ),
        SizedBox(
          height: 40,
        ),
        RaisedButton(
          color: Colors.white,
          onPressed: () async {
            try {
              DateTime newDate = await showMonthPicker(
                  context: context, initialDate: chosenDate, firstDate: DateTime.now());
              if (newDate == null) newDate = DateTime.now();
              setState(() {
                chosenDate = newDate;
              });
            } catch (e) {}
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(DateFormat.yMMMM().format(chosenDate)),
              Icon(Icons.expand_more)
            ],
          ),
        ),
        SizedBox(
          height: 40,
        ),
        NavigationButton(widget.prevStep, () {
          widget.nextStep();
          User user = Provider.of<User>(context, listen: false);
          user.eduDate = chosenDate;
          user.updateData();
        })
      ],
    );
  }
}
