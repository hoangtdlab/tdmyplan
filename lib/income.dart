import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'nextprevbutton.dart';
import 'user.dart';

class Income extends StatefulWidget {
  final Function() nextStep, prevStep;

  Income(this.prevStep, this.nextStep);

  @override
  State<StatefulWidget> createState() => _IncomeState();
}

class _IncomeState extends State<Income> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context, listen: false);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "When's your annual total pre-tax income?",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
          ),
        ),
        SizedBox(
          height: 40,
        ),
        TextField(
          keyboardType: TextInputType.number,
          controller: controller,
          cursorColor: Colors.green,
          decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.attach_money,
                color: Colors.green,
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(50.0),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(50.0),
                ),
                borderSide: BorderSide(color: Colors.green),
              ),
              filled: true,
              hintStyle: TextStyle(color: Colors.grey[800]),
              fillColor: Colors.white70,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 10, horizontal: 20)),
        ),
        SizedBox(
          height: 40,
        ),
        NavigationButton(widget.prevStep, () {
          widget.nextStep();
          User user = Provider.of<User>(context);
          user.income = double.parse(controller.text);
          user.updateData();
        })
      ],
    );
  }
}
