import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'nextprevbutton.dart';
import 'user.dart';

class Education extends StatefulWidget {
  final Function() nextStep, prevStep;

  Education(this.prevStep, this.nextStep);

  @override
  State<StatefulWidget> createState() => _EducationState();
}

class _EducationState extends State<Education> {
  bool studying = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Are you currently attending or planning to attend a post-secondary program?",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
          ),
        ),
        SizedBox(
          height: 40,
        ),
        InkWell(
          onTap: () {
            setState(
              () {
                studying = true;
              },
            );
          },
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Checkbox(
                activeColor: Colors.green,
                value: studying,
                onChanged: (_) {
                  setState(
                    () {
                      studying = true;
                    },
                  );
                },
              ),
              Text(
                "Yes",
                style: TextStyle(fontWeight: FontWeight.w700),
              )
            ],
          ),
        ),
        InkWell(
          onTap: () {
            setState(
              () {
                studying = false;
              },
            );
          },
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Checkbox(
                activeColor: Colors.green,
                value: !studying,
                onChanged: (_) {
                  setState(
                    () {
                      studying = false;
                    },
                  );
                },
              ),
              Text(
                "No",
                style: TextStyle(fontWeight: FontWeight.w700),
              )
            ],
          ),
        ),
        SizedBox(
          height: 40,
        ),
        NavigationButton(
          widget.prevStep,
          () {
            widget.nextStep();
            User user = Provider.of<User>(context);
            user.updateData();
          },
        )
      ],
    );
  }
}
