import 'dart:math';

import 'package:flutter/foundation.dart';

import 'goal_data.dart';

class User with ChangeNotifier {
  static final currentUser = User();
  String name = "";
  String goalName = "";
  DateTime goalDate;
  double goalTarget;
  double goalSave;
  DateTime eduDate;
  double income;
  double rent;
  double tuition;
  List<GoalData> bigGoals = [], smallGoals = [];

  void updateData() {
    notifyListeners();
  }

  static GoalData makeGoal(
    String name,
    DateTime date,
    double target,
    double save,
  ) {
    int numMonth = (date.difference(DateTime.now()).inDays ~/ 30) + 1;
    return GoalData(
      name,
      date,
      target,
      save,
      max(0, (target - save) / numMonth),
    );
  }

  GoalData currentGoal() {
    return makeGoal(goalName, goalDate, goalTarget, goalSave);
  }

  void updateBigGoal() {
    bigGoals.add(currentGoal());
  }

  void updateSmallGoal() {
    smallGoals.add(currentGoal());
  }
}
