import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'nextprevbutton.dart';
import 'user.dart';

class GoalTarget extends StatefulWidget {
  final Function() nextStep, prevStep;

  GoalTarget(this.prevStep, this.nextStep);

  @override
  State<StatefulWidget> createState() => _GoalTargetState();
}

class _GoalTargetState extends State<GoalTarget> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context, listen: false);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        RichText(
          text: TextSpan(
            text: 'You want to ',
            style: DefaultTextStyle.of(context).style.copyWith(
                  fontWeight: FontWeight.w700,
                  fontSize: 20,
                ),
            children: <TextSpan>[
              TextSpan(
                text: (user.goalName.length > 0)
                    ? '${user.goalName[0].toLowerCase() + user.goalName.substring(1)}'
                    : '',
                style: DefaultTextStyle.of(context).style.copyWith(
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                      color: Colors.green,
                    ),
              ),
              TextSpan(
                text: ' by ',
                style: DefaultTextStyle.of(context).style.copyWith(
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                    ),
              ),
              TextSpan(
                text: user.goalDate == null
                    ? ''
                    : DateFormat.yMMMM().format(user.goalDate),
                style: DefaultTextStyle.of(context).style.copyWith(
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                      color: Colors.green,
                    ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 40,
        ),
        Text(
          "How much do you need to save for it?",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 16,
          ),
        ),
        SizedBox(
          height: 8,
        ),
        TextField(
          keyboardType: TextInputType.number,
          controller: controller,
          cursorColor: Colors.green,
          decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.attach_money,
                color: Colors.green,
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(50.0),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(50.0),
                ),
                borderSide: BorderSide(color: Colors.green),
              ),
              filled: true,
              hintStyle: TextStyle(color: Colors.grey[800]),
              fillColor: Colors.white70,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 10, horizontal: 20)),
        ),
        SizedBox(
          height: 40,
        ),
        NavigationButton(widget.prevStep, () {
          widget.nextStep();
          User user = Provider.of<User>(context);
          user.goalTarget = double.parse(controller.text);
          user.updateData();
        })
      ],
    );
  }
}
