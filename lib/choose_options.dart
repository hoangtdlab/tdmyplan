import 'package:flutter/material.dart';

class MultiChoices extends StatefulWidget {
  List<String> choices;

  MultiChoices(this.choices);

  @override
  State<StatefulWidget> createState() => _MultiChoicesState();
}

class _MultiChoicesState extends State<MultiChoices> {
  String choice;

  @override
  Widget build(BuildContext context) {
    final choicesWidget = widget.choices.map((content) {
      return Padding(
        padding: EdgeInsets.only(right: 16.0),
        child: ButtonTheme(
          height: 30.0,
          child: MaterialButton(
            textColor: choice == content ? Colors.white : Colors.green,
            color: choice == content ? Colors.green : Colors.white,
            shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.green),
              borderRadius: BorderRadius.circular(30.0),
            ),
            child: Text(content),
            onPressed: () {
              setState(() {
                choice = content;
              });
            },
          ),
        ),
      );
    }).toList();
    return Row(
      children: choicesWidget,
    );
  }
}
