import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'goal_card.dart';
import 'goal_data.dart';
import 'goal_dialog.dart';
import 'router.dart';
import 'user.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int tab = 0;

  bool Function() _showDialog(GoalData data) {
    // flutter defined function
    return () {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return GoalDialog(data);
        },
      );
    };
  }

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context);
    final bigGoals = user.bigGoals.map(
      (data) {
        return Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: InkWell(
            onTap: _showDialog(data),
            child: GoalCard(data),
          ),
        );
      },
    );
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        onTap: (num) {
          setState(() {
            tab = num;
          });
        },
        currentIndex: tab, // this will be set when a new tab is tapped
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home),
            title: new Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Profile'),
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          vertical: 24,
          horizontal: 24,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              tab == 0 ? "Home" : "Profile",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 24,
              ),
            ),
            if (tab == 1)
              Image.asset(
                'assets/profile.png',
              ),
            if (tab == 0)
              Expanded(
                child: ListView(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Goals",
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 16,
                          ),
                        ),
                        ButtonTheme(
                          height: 23.0,
                          minWidth: 23,
                          child: RaisedButton(
                            child: Icon(
                              Icons.add,
                              color: Colors.white,
                              size: 20,
                            ),
                            onPressed: () {
                              Navigator.pushNamed(
                                  context, RouteNames.addGoalBig);
                            },
                            shape: CircleBorder(),
                            color: Colors.green,
                          ),
                        ),
                      ],
                    ),
                    ...bigGoals
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }
}
