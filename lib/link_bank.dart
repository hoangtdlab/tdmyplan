import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'nextprevbutton.dart';
import 'user.dart';

class LinkBank extends StatefulWidget {
  final Function() nextStep, prevStep;

  LinkBank(this.prevStep, this.nextStep);

  @override
  State<StatefulWidget> createState() => _LinkBankState();
}

class _LinkBankState extends State<LinkBank> {
  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context, listen: false);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Ok ${user.name}, let's get you synced up.",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
          ),
        ),
        SizedBox(
          height: 32,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 32.0),
          child: ButtonTheme(
            minWidth: double.infinity,
            child: RaisedButton(
              textColor: Colors.white,
              color: Colors.green,
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.green),
                borderRadius: BorderRadius.circular(30.0),
              ),
              child: Text("Link Bank Account"),
              onPressed: () {},
            ),
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 32.0),
          child: ButtonTheme(
            minWidth: double.infinity,
            child: RaisedButton(
              textColor: Colors.white,
              color: Colors.green,
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.green),
                borderRadius: BorderRadius.circular(30.0),
              ),
              child: Text("Link Visa Debit or Credit Card"),
              onPressed: () {},
            ),
          ),
        ),
        SizedBox(
          height: 40,
        ),
        NavigationButton(widget.prevStep, () {
          widget.nextStep();
          User user = Provider.of<User>(context);
          user.updateData();
        })
      ],
    );
  }
}
