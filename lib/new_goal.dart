import 'package:flutter/material.dart';

import 'first_goal.dart';
import 'goal_date.dart';
import 'goal_name.dart';
import 'goal_save.dart';
import 'goal_target.dart';
import 'router.dart';

class AddGoal extends StatefulWidget {
  bool isBig;

  AddGoal({this.isBig = true});

  @override
  State<StatefulWidget> createState() => _AddGoalState();
}

class _AddGoalState extends State<AddGoal> {
  int step = 0;
  final steps = [
    (prevStep, nextStep) => GoalName(prevStep, nextStep, isFirst: false),
    (prevStep, nextStep) => GoalDate(prevStep, nextStep),
    (prevStep, nextStep) => GoalTarget(prevStep, nextStep),
    (prevStep, nextStep) => GoalSave(prevStep, nextStep),
    (prevStep, nextStep) => FirstGoal(prevStep, nextStep),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        bottom: PreferredSize(
          preferredSize: Size(double.infinity, 1.0),
          child: LinearProgressIndicator(
            value: step / steps.length,
            backgroundColor: Colors.grey,
            valueColor: AlwaysStoppedAnimation<Color>(Colors.black38),
          ),
        ),
        centerTitle: true,
        title: Text(
          'MyPlan',
          style: TextStyle(
            fontWeight: FontWeight.w700,
          ),
        ),
        backgroundColor: Colors.green,
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          vertical: 24,
          horizontal: 24,
        ),
        child: Container(
          key: Key("Step " + step.toString()),
          child: steps[step](
            (step == 0)
                ? () {
                    Navigator.pop(context);
                  }
                : () {
                    setState(() {
                      step = step - 1;
                    });
                  },
            () {
              if (step == steps.length - 1) {
                Navigator.pushReplacementNamed(context, RouteNames.home);
              } else {
                setState(() {
                  step = step + 1;
                });
              }
            },
          ),
        ),
      ),
    );
  }
}
