import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'goal_card.dart';
import 'nextprevbutton.dart';
import 'user.dart';

class FirstGoal extends StatefulWidget {
  final Function() nextStep, prevStep;

  FirstGoal(this.prevStep, this.nextStep);

  @override
  State<StatefulWidget> createState() => _FirstGoalState();
}

class _FirstGoalState extends State<FirstGoal> {
  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "You created a new goal!",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
          ),
        ),
        SizedBox(
          height: 40,
        ),
        GoalCard(user.currentGoal(), template: true,),
        SizedBox(
          height: 40,
        ),
        NavigationButton(widget.prevStep, () {
          widget.nextStep();
          User user = Provider.of<User>(context, listen: false);
          user.updateData();
          user.updateBigGoal();
        })
      ],
    );
  }
}
