import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'nextprevbutton.dart';
import 'user.dart';

class GoalName extends StatefulWidget {
  final Function() nextStep, prevStep;
  bool isFirst;

  GoalName(this.prevStep, this.nextStep, {this.isFirst = true});

  @override
  State<StatefulWidget> createState() => _GoalNameState();
}

class _GoalNameState extends State<GoalName> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context, listen: false);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          widget.isFirst
              ? "Hi ${user.name}, let's create your first goal."
              : "Hi ${user.name}, let's create a new goal.",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Text(
          "You will be able to track your progress and build towards completing each goal.",
        ),
        SizedBox(
          height: 8,
        ),
        Text(
          "What do you want to achieve?",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 16,
          ),
        ),
        SizedBox(
          height: 8,
        ),
        TextField(
          controller: controller,
          cursorColor: Colors.green,
          decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(50.0),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(50.0),
                ),
                borderSide: BorderSide(color: Colors.green),
              ),
              filled: true,
              hintStyle: TextStyle(color: Colors.grey[800]),
              fillColor: Colors.white70,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 10, horizontal: 20)),
        ),
        SizedBox(
          height: 40,
        ),
        NavigationButton(widget.prevStep, () {
          widget.nextStep();
          User user = Provider.of<User>(context);
          user.goalName = controller.text;
          user.updateData();
        })
      ],
    );
  }
}
