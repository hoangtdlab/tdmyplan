import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'choose_options.dart';
import 'nextprevbutton.dart';
import 'user.dart';

class Tuition extends StatefulWidget {
  final Function() nextStep, prevStep;

  Tuition(this.prevStep, this.nextStep);

  @override
  State<StatefulWidget> createState() => _TuitionState();
}

class _TuitionState extends State<Tuition> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "How much do you spend on tuition?",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
          ),
        ),
        SizedBox(
          height: 8,
        ),
        TextField(
          keyboardType: TextInputType.number,
          controller: controller,
          cursorColor: Colors.green,
          decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.attach_money,
                color: Colors.green,
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(50.0),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(50.0),
                ),
                borderSide: BorderSide(color: Colors.green),
              ),
              filled: true,
              hintStyle: TextStyle(color: Colors.grey[800]),
              fillColor: Colors.white70,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 10, horizontal: 20)),
        ),
        SizedBox(
          height: 8,
        ),
        MultiChoices([
          "Per Term",
          "Per Year",
        ]),
        SizedBox(
          height: 40,
        ),
        NavigationButton(widget.prevStep, () {
          widget.nextStep();
          User user = Provider.of<User>(context);
          user.tuition = double.parse(controller.text);
          user.updateData();
        })
      ],
    );
  }
}
