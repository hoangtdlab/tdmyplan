class GoalData {
  String name = "";
  DateTime date;
  double target;
  double save;
  double suggested;
  bool isAutoDeposit = false;
  bool haveDeposited = false;

  void deposit() {
    save = save + suggested;
    haveDeposited = true;
  }

  GoalData(this.name, this.date, this.target, this.save, this.suggested, {this.isAutoDeposit = false});
}
