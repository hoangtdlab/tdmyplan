import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'user.dart';
import 'nextprevbutton.dart';

class NameChange extends StatefulWidget {
  final Function() nextStep, prevStep;

  NameChange(this.prevStep, this.nextStep);

  @override
  State<StatefulWidget> createState() => _NameChangeState();
}

class _NameChangeState extends State<NameChange> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "What's your name?",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 16,
          ),
        ),
        SizedBox(
          height: 40,
        ),
        TextField(
          controller: controller,
          cursorColor: Colors.green,
          decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(50.0),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(50.0),
                ),
                borderSide: BorderSide(color: Colors.green),
              ),
              filled: true,
              hintStyle: TextStyle(color: Colors.grey[800]),
              fillColor: Colors.white70,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 10, horizontal: 20)),
        ),
        SizedBox(
          height: 32,
        ),
        NavigationButton(widget.prevStep, () {
          widget.nextStep();
          User user = Provider.of<User>(context, listen: false);
          user.name = controller.text;
          user.updateData();
        })
      ],
    );
  }
}
